from models import traveler
from models import travelerToWizard
from models import wizard

def insertTravelerToWizard():
    travelerToWizard.TravelerToWizard().addConnection(wizardId='w1',travelerId='t1')
    travelerToWizard.TravelerToWizard().addConnection(wizardId='w1',travelerId='t2')
    travelerToWizard.TravelerToWizard().addConnection(wizardId='w2',travelerId='t3')

def insertWizard():
    wizard.Wizard().addWizard(id='w2',username='w2', isAvailable=True)
    wizard.Wizard().addWizard(id='w1',username='w1', isAvailable=True)

def insertTraveler():
    traveler.Traveler().addTraveler(id='t1',username='t1')
    traveler.Traveler().addTraveler(id='t2',username='t2')