from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_restful import reqparse, abort, Api, Resource
from database.sequel import *
from functools import wraps
from flask import g
from flask import request
from config import database
from bootstrap import *

(api, app) = start()

from models import traveler
from models import travelerToWizard
from models import wizard

parser = reqparse.RequestParser()

def authorize(f):
    @wraps(f)
    def func_wrapper(*args, **kwargs):
        try:
            user = request.headers['user']
            password = request.headers['password']
            if user == 'admin' and password == 'admin':
                g.user = user
            else:
                abort(404, message="User not found")
        except Exception as inst:
            abort(404, message="User and Password required")
        return f(*args, **kwargs)
    return func_wrapper

class TravelerToWizard(Resource):
    def post(self, travelerId, wizardId):
        travelerToWizard.TravelerToWizard().updateTimestamp(wizardId=wizardId, travelerId=travelerId)
        return { 'success': True }, 200

class WizardToTraveler(Resource):
    def post(self, travelerId, wizardId):
        travelerToWizard.TravelerToWizard().updateWizard(wizardId=wizardId, travelerId=travelerId)
        return { 'success': True }, 200

class AssignWizard(Resource):
    def post(self, travelerId):
        response = {}
        availableWizardId = wizard.Wizard().getAvailableWizard()

        if (len(availableWizardId) > 0):
            travelerToWizard.TravelerToWizard().addConnection(wizardId=availableWizardId[0][0],travelerId=travelerId)
            response['success'] = True
        else:
            response['success'] = False
            response['message'] = 'Please try again later'

        return response, 200

class AdminAssignWizard(Resource):
    method_decorators = [authorize]

    def post(self, travelerId, wizardId):
        travelerToWizard.TravelerToWizard().addConnection(wizardId=wizardId,travelerId=travelerId)
        return { 'success': True }, 200

class WizardSignout(Resource):
    def post(self, wizardId):
        wizard.Wizard().signout(wizardId)
        return { 'success': True }, 200

class WizardSignin(Resource):
    def post(self, wizardId):
        wizard.Wizard().signin(wizardId)
        return { 'success': True }, 200


# insertTravelerToWizard()
api.add_resource(
    TravelerToWizard,
    '/message/traveler/<travelerId>/wizard/<wizardId>',
)

api.add_resource(
    WizardToTraveler,
    '/message/wizard/<wizardId>/traveler/<travelerId>'
)

api.add_resource(
    AssignWizard,
    '/assign/traveler/<travelerId>'
)

api.add_resource(
    AdminAssignWizard,
    '/assign/traveler/<travelerId>/wizard/<wizardId>'
)

api.add_resource(
    WizardSignout,
    '/signout/wizard/<wizardId>'
)

api.add_resource(
    WizardSignin,
    '/signin/wizard/<wizardId>'
)


if __name__ == '__main__':
    app.run(debug=True)
