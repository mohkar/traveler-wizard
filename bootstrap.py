from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_restful import reqparse, abort, Api, Resource
from database.sequel import *
from functools import wraps
from flask import g
from flask import request
from config import database

def start():
    app = Flask(__name__)
    app.config['SQLALCHEMY_DATABASE_URI'] = database.postgresURL
    api = Api(app)
    db = SQLAlchemy(app)
    sequel = Sequel()
    sequel.initializeDB(db)
    return (api, app)