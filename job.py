from apscheduler.schedulers.blocking import BlockingScheduler
from database.sequel import *
from sqlalchemy import create_engine
from config import database
from bootstrap import *

(api, app) = start()

from models import travelerToWizard

def markTravelersInactive():
    print("marking travelers inactive")
    travelerToWizard.TravelerToWizard().markTravelersInactive()

sched = BlockingScheduler()
sched.add_job(markTravelersInactive, 'interval', seconds=2)
sched.start()

