from database.sequel import *
from .travelerToWizard import TravelerToWizardModel
from sqlalchemy.orm import relationship
from sqlalchemy import func
from sqlalchemy.sql import label
from config import misc

db = Sequel().getDB()

class WizardModel(db.Model):
    __tablename__ = 'wizard'
    id = db.Column(db.String(80), primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    isAvailable = db.Column(db.Boolean, unique=False, nullable=False)

    wizardIdR = relationship('TravelerToWizardModel', backref='TravelerToWizardModel.wizardId', primaryjoin='WizardModel.id==TravelerToWizardModel.wizardId', lazy='joined')

    def __repr__(self):
        return '<Wizard %r>' % self.username

class Wizard():
    def addWizard(self, id, username, isAvailable):
        wizard = WizardModel(id=id, username=username, isAvailable=isAvailable)
        db.session.add(wizard)
        db.session.commit()

    def getAvailableWizard(self):
        wizardList = db.session.query().filter(
            WizardModel.id == TravelerToWizardModel.wizardId,
            WizardModel.isAvailable == True
        ).add_columns(
            WizardModel.id,
            label('total', func.count(TravelerToWizardModel.wizardId))
        ).group_by(
            TravelerToWizardModel.wizardId,
            WizardModel.id
        ).having(
            func.count(TravelerToWizardModel.wizardId) < misc.maxTravelerCountPerWizard
        ).limit(1).all()
        print(wizardList)
        return wizardList

    def signout(self, wizardId):
        WizardModel.query.filter_by(wizardId=wizardId).update(isAvailable=False)

    def signin(self, wizardId):
        WizardModel.query.filter_by(wizardId=wizardId).update(isAvailable=True)

