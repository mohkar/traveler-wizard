from database.sequel import *

db = Sequel().getDB()

class TravelerModel(db.Model):
    __tablename__ = 'traveler'
    id = db.Column(db.String(80), primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    isActive = db.Column(db.Boolean, unique=False, nullable=False)

    def __repr__(self):
        return '<Traveler %r>' % self.username

class Traveler():
    def addTraveler(self, id, username):
        traveler = TravelerModel(id=id, username=username, isActive=True)
        db.session.add(traveler)
        db.session.commit()

    def markTravelersInactive(self, travelerIds):
        batchSize = 1000
        count = 0
        for travelerId in travelerIds:
            updatedTravelerRec = TravelerModel.query.filter_by(id=travelerId[0]).all()[0]
            updatedTravelerRec.isActive = False
            db.session.add(updatedTravelerRec)
            if (count%batchSize == 0):
                db.session.commit()
                count = 0
            count += 1
