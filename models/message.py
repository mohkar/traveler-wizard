from database.sequel import *

db = Sequel().getDB()

class MessageModel(db.Model):
    __tablename__ = 'message'
    id = db.Column(db.Integer, primary_key=True)
    travelerId = db.Column(db.String(80), unique=True, nullable=False)

    def __repr__(self):
        return '<Message %r>' % self.id

class Message():
    def addMessage(self, travelerId):
        message = MessageModel(travelerId=travelerId)
        db.session.add(message)
        db.session.commit()

    getMessageHistory(self, travelerId):
        return MessageModel.query.filter_by(travelerId=travelerId).all()
