from database.sequel import *
from .traveler import Traveler, TravelerModel
import time

db = Sequel().getDB()

class TravelerToWizardModel(db.Model):
    __tablename__ = 'traveler_to_wizard'
    id=db.Column(db.Integer, primary_key=True)
    wizardId = db.Column(db.String(80), db.ForeignKey("wizard.id"))
    travelerId = db.Column(db.String(80))
    lastMessageTimestamp = db.Column(db.Integer, unique=False, nullable=False)


    def __repr__(self):
        return '<TravelerToWizard %r>' % (self.wizardId + self.travelerId)

class TravelerToWizard():
    def addConnection(self, wizardId, travelerId):
        traveler = TravelerToWizardModel(wizardId=wizardId, travelerId=travelerId, lastMessageTimestamp=int(time.time()))
        db.session.add(traveler)
        db.session.commit()

    def updateTimestamp(self, wizardId, travelerId):
        updatedTimestamp = TravelerToWizardModel.query.filter_by(wizardId=wizardId, travelerId=travelerId).update(dict(lastMessageTimestamp=int(time.time())))
        db.session.commit(updatedTimestamp)

    def markTravelersInactive(self):
        inactiveTravelers = TravelerToWizardModel.query.with_entities(TravelerToWizardModel.travelerId).filter(TravelerToWizardModel.lastMessageTimestamp < (int(time.time()) - 600)).all()
        Traveler.markTravelersInactive(self, inactiveTravelers)

    def updateWizard(self, travelerId, wizardId):
        updatedTimestamp = TravelerToWizardModel.query.filter_by(travelerId=travelerId).update(dict(wizardId=wizardId))
        db.session.commit(updatedTimestamp)